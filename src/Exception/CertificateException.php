<?php
namespace Drupal\commerce_wechat\Exception;

/**
 * 提供一个微信支付平台证书异常类
 *
 * Class CertificateException
 *
 * @package Drupal\commerce_wechat\Exception
 */
class CertificateException extends \RuntimeException {}
