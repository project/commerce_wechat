<?php
/**
 * 显示退款结果，如果退款未成功则将进行主动退款查询，结果会被记录，然后再显示
 * by:yunke
 * email:phpworld@qq.com
 * Wechat ID:indrupal
 */

namespace Drupal\commerce_wechat\Controller;

use Drupal\Core\Controller\ControllerBase;


use Drupal\Core\Url;
use Drupal\commerce_wechat\WechatAPI;
use Drupal\commerce_wechat\OrderIDConverter;
use Drupal\commerce_wechat\Plugin\Commerce\PaymentGateway\Wechat;
use Drupal\Core\Render\Markup;


class QueryRefund extends ControllerBase {

  protected $logger = NULL;

  public function __construct() {
    $this->logger = $this->getLogger('commerce_wechat');
  }

  /**
   * 显示退款结果
   *
   * @param $orderID    string 订单ID
   * @param $paymentID  string 支付ID
   * @param $refundID   string 退款ID
   *
   * @return array 退款结果渲染数组
   */
  public function index($orderID, $paymentID, $refundID) {
    /**
     * 退款异步通知和退款主动查询均会对退款状态进行存档操作，这将影响支付状态
     * 为避免可能的并发发生争抢导致数据问题（比如双退款），必须进行锁控制
     *
     * @see \Drupal\commerce_wechat\Plugin\Commerce\PaymentGateway\Wechat::onRefundNotify
     */
    $lock = \Drupal::lock();
    $operationID = 'refund_' . $paymentID . '_' . $refundID;
    $isGetLock = FALSE;
    $isGetLock = $lock->acquire($operationID);
    if (!$isGetLock) {
      if (!$lock->wait($operationID, 5)) {//等待五秒继续获取锁
        $isGetLock = $lock->acquire($operationID);
      }
    }
    if ($isGetLock) {
      $result = $this->queryRefund($orderID, $paymentID, $refundID);
      $lock->release($operationID);
      return $result;
    }
    $this->messenger()->addStatus('Processing asynchronous refund, please wait...');
    $returnLink = [
      '#title' => $this->t('Return'),
      '#type'  => 'link',
      '#url'   => Url::fromRoute('entity.commerce_payment.operation_form', ['commerce_order' => $orderID, 'commerce_payment' => $paymentID, 'operation' => 'view_refund'], ['attributes' => ['class' => ['button']],]),
    ];
    return $returnLink;
  }

  /**
   * 退款信息查询，如果退款尚未成功则主动进行一次查询操作
   */
  public function queryRefund($orderID, $paymentID, $refundID) {
    $returnLink = [
      '#title' => $this->t('Return'),
      '#type'  => 'link',
      '#url'   => Url::fromRoute('entity.commerce_payment.operation_form', ['commerce_order' => $orderID, 'commerce_payment' => $paymentID, 'operation' => 'view_refund'], ['attributes' => ['class' => ['button']],]),
    ];
    $payment = \Drupal::entityTypeManager()->getStorage('commerce_payment')->load((int) $paymentID);
    if (empty($payment)) {
      $this->messenger()->addWarning($this->t('Payment entity(ID:@paymentID) does not exist', ['@paymentID' => $paymentID]));
      return $returnLink;
    }
    $gatewayEntity = $payment->getPaymentGateway(); //支付网关配置实体
    $config = $gatewayEntity->getPluginConfiguration();//插件配置，即微信API接口配置
    $orderEntity = $payment->getOrder(); //订单实体
    if ($orderID != $orderEntity->id()) {
      //验证支付实体所属对应订单，避免人为返回一个其他订单的支付实体
      $this->messenger()->addWarning($this->t('payment entity ID:@paymentID and order ID:@orderID do not match', ['@paymentID' => $paymentID, '@orderID' => $orderID]));
      return $returnLink;
    }
    $moduleData = $orderEntity->getData(COMMERCE_WECHAT_DATA_KEY, []);
    if (!isset($moduleData['refund'][$paymentID][$refundID])) {
      $this->messenger()->addWarning(
        $this->t('Refund ID(@refundID) of payment(@paymentID) does not exist',
          ['@refundID' => $refundID, '@paymentID' => $paymentID])
      );
      return $returnLink;
    }
    $refundData = $moduleData['refund'][$paymentID][$refundID];
    if ($refundData['state'] == Wechat::REFUND_SUCCESS) {//已经退款成功的不再进行查询
      return ['display' => $this->getDisplay($refundData), 'link' => $returnLink];
    }
    /**
     * 如果是退款不成功，或者是尚未收到异步通知的新建状态，则进行一次主动查询
     */
    $refundNumber = OrderIDConverter::toWechat($paymentID, $config['systemId']) . '_' . $refundID;
    $wechatAPI = new WechatAPI($config, $this->logger);
    $result = $wechatAPI->queryRefund($refundNumber);
    if ($result == FALSE) {
      $this->messenger()->addWarning('Refund query failed');
      return $returnLink;
    }

    if ($result['status'] == 'SUCCESS'
      && $result['amount']['payer_refund'] == $refundData['refund_amount']
    ) {
      //可认为退款成功了
      $refundData['state'] = Wechat::REFUND_SUCCESS;
      $refundData['data'] = $result;
      $amount = \Drupal::service('commerce_price.minor_units_converter')->fromMinorUnits($refundData['refund_amount'], $refundData['currency']);
      $old_refunded_amount = $payment->getRefundedAmount();
      if (!empty($old_refunded_amount)) {
        $new_refunded_amount = $amount->add($old_refunded_amount);
      }
      else {
        $new_refunded_amount = $amount;
      }
      if ($new_refunded_amount->lessThan($payment->getAmount())) {
        $payment->state = 'partially_refunded';
      }
      else {
        $payment->state = 'refunded';
      }
      $payment->setRefundedAmount($new_refunded_amount);
      $moduleData['refund'][$paymentID][$refundID] = $refundData;
      $orderEntity->setData(COMMERCE_WECHAT_DATA_KEY, $moduleData);
      $orderEntity->save();
      $payment->save();
    }
    else {
      $refundData['state'] = Wechat::REFUND_FAIL;
      $refundData['data'] = $result;
      $moduleData['refund'][$paymentID][$refundID] = $refundData;
      $orderEntity->setData(COMMERCE_WECHAT_DATA_KEY, $moduleData);
      $orderEntity->save();
      //恢复支付实体的状态，否则如果一直挂起，那么无法重新发起退款
      $refundedAmount = $payment->getRefundedAmount();
      if (empty($refundedAmount) || $refundedAmount->isZero()) {
        $payment->state = 'completed';
      }
      elseif ($refundedAmount->lessThan($payment->getAmount())) {
        $payment->state = 'partially_refunded';
      }
      else {
        $payment->state = 'refunded';
      }
      $payment->save();
    }
    return ['display' => $this->getDisplay($refundData), 'link' => $returnLink];
  }

  /**
   * 返回查询后的退款数据渲染数组
   *
   * @param $refundData
   *
   * @return array 退款数据渲染数组
   */
  protected function getDisplay($refundData) {
    $state = [
      Wechat::REFUND_SUCCESS => $this->t('SUCCESS'),
      Wechat::REFUND_NEW     => $this->t('NEW', [], ['context' => 'refund state']),
      Wechat::REFUND_FAIL    => $this->t('FAIL'),
    ];
    $refund = [
      '#type'       => 'table',
      '#caption'    => $this->t('Refund Query Result'),
      '#header'     => [
        $this->t('state'),
        $this->t('amount'),
        $this->t('currency'),
        $this->t('wechat raw return'),
      ],
      '#empty'      => $this->t('No Result'),
      '#sticky'     => TRUE,
      '#attributes' => ['class' => ['commerce-wechat-refund-query-result']],
    ];
    $refund[0]['state'] = [
      '#markup' => $state[$refundData['state']],
    ];
    $priceConverter = \Drupal::service('commerce_price.minor_units_converter');
    $refund[0]['amount'] = [
      '#markup' => (string) $priceConverter->fromMinorUnits($refundData['refund_amount'], $refundData['currency']),
    ];
    $refund[0]['currency'] = [
      '#markup' => $refundData['currency'],
    ];
    $refund[0]['data'] = ['#markup' => Markup::create('<pre>' . print_r($refundData['data'], TRUE) . '</pre>'),];
    return $refund;
  }

}
