<?php

namespace Drupal\commerce_wechat\Form;

use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\PluginForm\PaymentGatewayFormBase;
use Drupal\Core\Render\Markup;

class PaymentRefundForm extends PaymentGatewayFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    $form['#success_message'] = Markup::create($this->t('Refund processing...') . '<a href="javascript:window.location.reload()" class="button">' . $this->t('refresh') . '</a>');
    $form['amount'] = [
      '#type'                 => 'commerce_price',
      '#title'                => $this->t('Amount'),
      '#default_value'        => $payment->getBalance()->toArray(),
      '#required'             => TRUE,
      '#available_currencies' => [$payment->getAmount()->getCurrencyCode()],
    ];
    $form['message'] = [
      '#markup' => $this->t('Multiple refunds are supported for a single payment, but no more than 50 and one year'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);
    $amount = Price::fromArray($values['amount']);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $balance = $payment->getBalance();
    if ($amount->greaterThan($balance)) {
      $form_state->setError($form['amount'], $this->t("Can't refund more than @amount.", ['@amount' => $balance->__toString()]));
    }
    if ($amount->isZero()) {//不允许进行0金额退款
      $form_state->setError($form['amount'], $this->t("The refund amount cannot be 0"));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);
    $amount = Price::fromArray($values['amount']);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;
    $payment_gateway_plugin->refundPayment($payment, $amount);
  }

}
