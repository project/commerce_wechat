<?php

/**
 *  由于微信支付的平台证书会不定期更新 且只能通过API接口获取 因此需要自动处理
 *  本类用于获取微信支付平台证书
 *  by:yunke
 *  email:phpworld@qq.com
 */

namespace Drupal\commerce_wechat\Certificate;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\commerce_wechat\Exception\CertificateException;
use WechatPay\GuzzleMiddleware\WechatPayMiddleware;
use WechatPay\GuzzleMiddleware\Util\PemUtil;
use Drupal\commerce_wechat\Certificate\NoopValidator;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Client;
use WechatPay\GuzzleMiddleware\Util\AesUtil;
use WechatPay\GuzzleMiddleware\Auth\WechatPay2Validator;
use WechatPay\GuzzleMiddleware\Auth\CertificateVerifier;
use GuzzleHttp\Exception\RequestException;

class GetWechatCertificate {

  //微信接口配置数据
  protected $wechatConfig = NULL;

  //日志服务
  protected $logger = NULL;

  //平台证书API接口
  protected $apiURL = 'https://api.mch.weixin.qq.com/v3/certificates';


  public function __construct(array $config, LoggerChannelInterface $logger = NULL) {
    $this->wechatConfig = $config;
    if ($logger) {
      $this->logger = $logger;
    }
    else {
      $this->logger = \Drupal::logger('commerce_wechat');
    }
    $apiURL = $this->wechatConfig['api' ]['certificatesURL']; //平台证书API接口
    if ($apiURL) {
      $this->apiURL = $apiURL;
    }
  }

  /**
   * @return array 成功时返回数组
   * @throws \Drupal\commerce_wechat\Exception\CertificateException 失败时抛出异常
   */
  public function getCertificate() {
    $message = '';//日志消息
    try {
      // 商户相关配置
      $merchantId = $this->wechatConfig['merchantId']; // 商户号
      $merchantSerialNumber = $this->wechatConfig['merchantSerialNumber']; // 商户API证书序列号
      $merchantPrivateKey = PemUtil::loadPrivateKeyFromString($this->wechatConfig['merchantPrivateKey']); // 商户私钥

      // 构造一个WechatPayMiddleware
      $builder = WechatPayMiddleware::builder()
        ->withMerchant($merchantId, $merchantSerialNumber, $merchantPrivateKey); // 传入商户相关配置

      $builder->withValidator(new NoopValidator); // 临时"跳过”应答签名的验证
      $wechatpayMiddleware = $builder->build();

      // 将WechatPayMiddleware添加到Guzzle的HandlerStack中
      $stack = HandlerStack::create();
      $stack->push($wechatpayMiddleware, 'wechatpay');
      // 创建Guzzle HTTP Client时，将HandlerStack传入
      $client = new Client(['handler' => $stack]);

      // 接下来，正常使用Guzzle发起API请求，WechatPayMiddleware会自动地处理签名和验签
      $resp = $client->request('GET', $this->apiURL, [
        'headers' => ['Accept' => 'application/json'],
      ]);
      if ($resp->getStatusCode() < 200 || $resp->getStatusCode() > 299) {
        $message = t("wechat platform certificates download failed, code= @StatusCode , body=[ @Body ]\n",
          [
            '@StatusCode' => $resp->getStatusCode(),
            '@Body'       => $resp->getBody(),
          ]);
        $this->logger->error($message);
        throw new CertificateException($message);
      }

      $list = json_decode($resp->getBody(), TRUE);

      $plainCerts = [];
      $x509Certs = [];

      $decrypter = new AesUtil($this->wechatConfig['apiV3SecretKey']); //api v3 密钥
      foreach ($list['data'] as $item) {
        $encCert = $item['encrypt_certificate'];
        $plain = $decrypter->decryptToString($encCert['associated_data'], $encCert['nonce'], $encCert['ciphertext']);
        if (!$plain) {
          $message = t("encrypted wechat platform certificate decrypt fail!");
          $this->logger->error($message);
          throw new CertificateException($message);
        }
        // 通过加载对证书进行简单合法性检验
        $cert = \openssl_x509_read($plain); // 从字符串中加载证书
        if (!$cert) {
          $message = t("downloaded wechat platform certificate check fail!");
          $this->logger->error($message);
          throw new CertificateException($message);
        }
        $plainCerts[] = $plain;
        $x509Certs[] = $cert;
      }
      // 使用下载的证书再来验证一次应答的签名
      $validator = new WechatPay2Validator(new CertificateVerifier($x509Certs));
      if (!$validator->validate($resp)) {
        $message = t("validate response fail using downloaded wechat platform certificates!"); //发生了中间人攻击
        $this->logger->error($message);
        throw new CertificateException($message);
      }
      $certificates = [];
      // 提取证书信息
      foreach ($list['data'] as $index => $item) {
        $certificate = [];
        $certificate['serial_no'] = $item['serial_no'];
        $certificate['effective_time'] = $item['effective_time'];
        $certificate['expire_time'] = $item['expire_time'];
        $certificate['certificate'] = $plainCerts[$index];
        $certificates[] = $certificate;
      }
      //$message = t("wechat platform certificates update successfully !"); //应该由调用方写入日志
      //$this->logger->info($message);
      return $certificates;

    } catch (RequestException $e) {
      $message = t("wechat platform certificates download failed, message=[ @Message ]",
        ['@Message' => $e->getMessage()]
      );
      if ($e->hasResponse()) {
        $message .= "code={$e->getResponse()->getStatusCode()}, body=[{$e->getResponse()->getBody()}]";
      }
      $this->logger->error($message);
      throw new CertificateException($message);
    } catch (\Exception $e) {
      $message = t("wechat platform certificates download failed, message=[ @Message ]",
        ['@Message' => $e->getMessage()]
      );
      $this->logger->error($message);
      throw new CertificateException($message);
    }

  }


}

