commerce_wechat
===
#overview
> Provide wechat payment integration for Drupal Commerce,Wechat Pay is the most popular payment method in China,The module has the following main features：
* The gateway automatically identifies payment scenario and call the corresponding API
* Support multiple systems can use same wechat api account, avoid order number repetition and unable to pay
* Customize the color and size of QR code for PC payment, and generate QR code quickly
* Automatic maintenance and update wechat payment platform certificate, foolproof management
* Automatically clean up abandoned payment entities, slimming database, and custom batch capacity
* Support multiple refunds for single payment, and record details
* Thanks to the architecture design, there will never be repeated payment order number
* The core directly uses wechat official SDK, no redundant tripartite library, high performance, security
* Customize payment expiration time
* High reliability, combined with the lock system, to eliminate the very low probability of multiple refund data errors
* Multi-language support, using English as the development meta language, without polluting the translation system
* The code documentation is annotated in as much detail as possible, giving full consideration to users' extended development needs

#Installation Requirement
Drupal 9, Drupal Commerce 2.X and above

#Install
Because the qrcode is required for payment on PC, so the module "yunke qrcode" needs to be installed first, and the official SDK package of wechat payment is required, it must be installed by Composer, Follow the steps:

```php
cd /root/mysite
composer require drupal/yunke_qrcode
composer require drupal/commerce_wechat
```

#About the author
This module is developed by Will-Nice (Shenzhen) Technology Co., Ltd<br>
[未来很美](http://www.will-nice.com,"Official Site") http://www.will-nice.com<br>
Developer: Yunke (phpworld@qq.com)<br>
Will-Nice is a dedicated Drupal development company, located in Shenzhen, China, if you have development needs, contact us come on

#Related modules
* [commerce_wechat_pay](https://www.drupal.org/project/commerce_wechat_pay)
* [commerce_cnpay](https://www.drupal.org/project/commerce_cnpay)
* [yunke_pay](https://www.drupal.org/project/yunke_pay)
* [commerce_alipayment](https://www.drupal.org/project/commerce_alipayment)

## The following is the Description in Chinese
#概述
> 本模块为Drupal电商提供微信支付集成，有如下主要特性：
* 网关自动识别支付场景，自动调用对应接口
* 支持同一接口账号下运行不限个系统，避免系统间订单号重复而无法下单
* 支持自定义PC端支付二维码的颜色、尺寸，且极速生成二维码
* 自动维护更新微信支付平台证书，傻瓜化管理
* 自动清理永久废弃的支付实体，为数据库瘦身，且批处理量可自定义
* 支持单笔支付发起多笔退款，并记录明细备查
* 永不出现支付单号重复而无法付款，这得益于架构设计
* 底层直接采用微信官方SDK，无冗余的三方库，高性能、安全，系统清爽
* 自定义支付超期时间，过期限制用户无法支付
* 高可靠，结合并发锁机制，杜绝极低概率下的多退款数据错误
* 多语言支持，以英语作为开发元语言，不污染翻译系统
* 代码文档注释尽可能详细，充分考虑用户的扩展开发需求

#安装需求
须Drupal 9及以上，Drupal Commerce 2.X以上

#安装
由于在PC端付款时需要二维码，因此需要先安装“yunke_qrcode”模块，由于需要微信官方SDK包的支持，所以必须采用composer方式安装以集成SDK到类加载器中，安装如下:

```php
cd /root/mysite
composer require drupal/yunke_qrcode
composer require drupal/commerce_wechat
```

#关于作者
本模块由“未来很美（深圳）科技有限公司”开发<br>
[未来很美](http://www.will-nice.com,"Official Site") http://www.will-nice.com<br>
开发者: 云客(phpworld@qq.com)<br>
未来很美科技是一家位于中国深圳的专注于Drupal开发的科技公司，如你有开发需求请联系我们

#相关模块
* [commerce_wechat_pay](https://www.drupal.org/project/commerce_wechat_pay)
* [commerce_cnpay](https://www.drupal.org/project/commerce_cnpay)
* [yunke_pay](https://www.drupal.org/project/yunke_pay)
* [commerce_alipayment](https://www.drupal.org/project/commerce_alipayment)


<br>
<br>

